import "./App.scss";
import { arrowBottom, bedPreview, list, plus, productColorPreview, starActive } from "./assets/img";
import AdminSidebarLeft from './components/AdminSidebarLeft';
import { Route, Switch } from "react-router";
import AdminProductEditingPage from "./adminPages/adminProductEditingPage/AdminProductEditingPage";
import AdminSigninPage from "./adminPages/AdminSignInPage/AdminSigninPage";
import {useSelector} from "react-redux";
import AdminOrdersPage from "./adminPages/AdminOrdersPage/AdminOrdersPage";
import {Redirect} from "react-router-dom";
import ClientsPage from "./adminPages/ClientsPage/ClientsPage";
import CurrentClient from "./adminPages/ClientsPage/CurrentClient";
import Categories from './adminPages/Categories/Categories';
import CategoryList from './adminPages/Categories/CategoryList';

function App() {
    const isAuth = useSelector(state => state.admin.accessToken);
  return (
    <main id='admin-page'>
      <Switch>
          <Route path='/' exact>
              <Redirect to='/admin/categories'/>
          </Route>
          <Route path='/admin/signin'>
              <AdminSigninPage isAuth={isAuth} />
          </Route>
          <Route path='/admin'>
              <AdminSidebarLeft isAuth={isAuth} />
              <Switch>
                  <Route path='/admin/categories'>
                      <Categories />
                  </Route>
                  <Route path='/admin/category/:id?'>
                      <CategoryList />
                  </Route>
                  <Route path='/admin/product-editing/:id?'>
                      <AdminProductEditingPage />
                  </Route>
                  <Route path='/admin/orders'>
                      <AdminOrdersPage />
                  </Route>
                  <Route exact path='/admin/clients'>
                      <ClientsPage />
                  </Route>
                  <Route path='/admin/clients/:client?'>
                      <CurrentClient />
                  </Route>
              </Switch>
          </Route>
      </Switch>
    </main>
  );
}

export default App;
