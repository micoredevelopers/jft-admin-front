import instance from "./axiosInstance";

export const categoriesAPI = {
	list(params) {
		return instance.get(`admin/api/categories?per_page=${params.per_page ?? ''}\n
		&page=${params.page ?? ''}\n
		&search=${params.search ?? ''}\n
		&sort=${params.sort ?? ''}\n
		&direction=${params.direction ?? ''}`);
	},
	products(id, params){
		return instance.get(`admin/api/category_products/${id}?per_page=${params.per_page ?? ''}\n
		&page=${params.page ?? ''}\n
		&search=${params.search ?? ''}\n
		&sort=${params.sort ?? ''}\n
		&direction=${params.direction ?? ''}`);
	},
	update(id, status){
		return instance.post(`admin/api/category/update/${id}?active=${status}`);
	},
}