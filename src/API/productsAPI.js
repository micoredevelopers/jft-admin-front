import instance from "./axiosInstance";

export const productsAPI = {
	list() {
		return instance.get(`admin/api/products?page=2`);
	},
	show(id){
		return instance.get(`admin/api/product/${id}`);
	},
	update(id, status){
		return instance.post(`admin/api/product/update/${id}?active=${status}`);
	}
}