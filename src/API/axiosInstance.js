import axios from "axios";

const instance = axios.create({
    baseURL: process.env.REACT_APP_API_URL,
});
instance.defaults.headers.common["Authorization"] =
    "Bearer " + localStorage.getItem("accessToken");

instance.interceptors.request.use(
    function (config) {
        const token = localStorage.getItem("accessToken");
        if (token) config.headers.Authorization = `Bearer ${token}`;
        return config;
    },
    function (error) {
        return Promise.reject(error);
    }
);
export default instance;

