import instance from "./axiosInstance";

export const authAPI = {
    login(data) {
        return instance.post("admin/login", data);
    },
    logout(data) {
        return instance.post("admin/logout", data);
    },
}