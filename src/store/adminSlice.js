import { createSlice } from "@reduxjs/toolkit";
import { authAPI } from "../API/adminAPI";

export const adminSlice = createSlice({
  name: "admin",
  initialState: {
    accessToken: localStorage.getItem("accessToken"),
    adminSigninErrorMessage: "",
  },
  reducers: {
    setToken: (state, action) => {
      state.accessToken = action.payload;
    },
    setAdminSigninErrorMessage: (state, action) => {
      state.adminSigninErrorMessage = action.payload;
    },
  },
});

export const { setToken, setAdminSigninErrorMessage } = adminSlice.actions;
export default adminSlice.reducer;

export const adminSignin = (data) => async (dispatch) => {
  try {
    const response = await authAPI.login(data);
    localStorage.setItem("accessToken", response.data.token);
    dispatch(setToken(response.data.token));
  } catch (error) {
    dispatch(setAdminSigninErrorMessage(error.response.data.error));
  }
};
export const adminLogout = () => async (dispatch) => {
  try {
    await authAPI.logout();
    localStorage.removeItem("accessToken");
    dispatch(setToken());
  } catch (error) {}
};
