import { configureStore } from '@reduxjs/toolkit'
import adminReducer from './adminSlice'
import productsSlice from './productsSlice';
import categoriesSlice from './categories';

export default configureStore({
    reducer: {
        admin: adminReducer,
        products: productsSlice,
        categories: categoriesSlice,
    },
})