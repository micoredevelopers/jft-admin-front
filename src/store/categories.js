import { createSlice } from '@reduxjs/toolkit';
import { categoriesAPI } from '../API/categories';

export const categoriesSlice = createSlice({
	name: 'categories',
	initialState: {
		categoriesList: [],
		products: [],
	},
	reducers: {
		contentChange: (state,action) => {
			state = {
				...state,
				[action.payload.name]: action.payload.value
			};
			return state;
		},
	},
});

export const {
	contentChange,
} = categoriesSlice.actions;
export default categoriesSlice.reducer;

export const categoriesListReducer = (params) => async (dispatch) => {
	try {
		const response = await categoriesAPI.list(params);
		dispatch(contentChange({name: 'categoriesList', value: response.data}));
	}catch(error){
		console.log(error);
	}
}
export const getCategoryProducts = (id, params) => async (dispatch) => {
	try {
		const response = await categoriesAPI.products(id, params);
		dispatch(contentChange({name: 'products', value: response.data}));
	}catch (error){
		console.log(error);
	}
}
export const updateCategoryReducer = (id, status) => async (dispatch) => {
	try {
		await categoriesAPI.update(id, status);
	}catch (error){
		console.log(error);
	}
}
