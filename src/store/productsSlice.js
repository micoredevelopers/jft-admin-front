import { createSlice } from '@reduxjs/toolkit';
import { productsAPI } from '../API/productsAPI';

export const productsSlice = createSlice({
	name: 'products',
	initialState: {
		list: [],
		currentProduct: {},
	},
	reducers: {
		setOrdersList: (state, action) => {
			state.list = action.payload;
		},
		setCurrentProduct: (state, action) => {
			state.currentProduct = action.payload;
		},
	},
});

export const {
	setOrdersList,
	setCurrentProduct
} = productsSlice.actions;
export default productsSlice.reducer;

export const ordersListReducer = () => async dispatch => {
	try {
		const response = await productsAPI.list();
		dispatch(setOrdersList(response.data));
	} catch (error) {
		console.log(error);
	}
};
export const showProductReducer = (id) => async dispatch => {
	try {
		const response = await productsAPI.show(id);
		dispatch(setCurrentProduct(response.data.data));
	} catch (error) {
		console.log(error);
	}
};
export const updateProductReducer = (id, status) => async dispatch => {
	try {
		await productsAPI.update(id, status);
	} catch (error) {
		console.log(error);
	}
};