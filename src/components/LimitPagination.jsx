import Select from 'react-select';
import React, { useEffect } from 'react';

const limitPaginationSelectOptions = [
	{ value: "16", label: "16"},
	{ value: "24", label: "24"},
	{ value: "32", label: "32"},
	{ value: "48", label: "48"},
]

const LimitPagination = ({defaultValue, onChange}) => {
	useEffect(() => {

	},[defaultValue]);
	return (
		<Select
			isSearchable={false}
			className='react_select_container'
			classNamePrefix='react-select-small'
			defaultValue={defaultValue ? { value: defaultValue, label: defaultValue } : limitPaginationSelectOptions[0]}
			onChange={onChange}
			options={limitPaginationSelectOptions}
		/>
	)
}
export default LimitPagination;