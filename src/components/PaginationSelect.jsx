import React from "react";
import Select from 'react-select';

const PaginationSelect = () => {
  return (
    <Select
      isSearchable={false}
      className='react_select_container'
      classNamePrefix='react-select-small'
      defaultValue={{ value: "16", label: "16", type: 1 }}
      onChange={(e) => {}}
      options={[
        { value: "16", label: "16", type: 1 },
        { value: "24", label: "24", type: 1 },
        { value: "32", label: "32", type: 1 },
        { value: "48", label: "48", type: 1 },
      ]}
    />
  );
};
export default PaginationSelect;
