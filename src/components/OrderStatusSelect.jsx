import React from "react";
import Select from "react-select";

const OrderStatusSelect = (options, ...props) => {
  const orderStatusOptions = [
    { value: "Новый", label: "Новый", type: 1, color: {text: '#fff', back: '#345DF0'} },
    { value: "Взял в работу", label: "Взял в работу", type: 2, color: {text: '#000', back: '#FABD63'} },
    { value: "В обработке", label: "В обработке", type: 3, color: {text: '#000', back: '#FABD63'} },
    { value: "Предзаказ", label: "Предзаказ", type: 4, color: {text: '#000', back: '#F2C533'} },
    { value: "На складе", label: "На складе", type: 5, color: {text: '#000', back: '#F2C533'} },
    { value: "Доставка", label: "Доставка", type: 6, color: {text: '#000', back: '#F2C533'} },
    { value: "Получено", label: "Получено", type: 7, color: {text: '#000', back: '#A1F1C1'} },
    { value: "Завершен", label: "Завершен", type: 8, color: {text: '#fff', back: '#3CE37F'} },
    { value: "Возврат", label: "Возврат", type: 9, color: {text: '#fff', back: '#F03434'} },
  ];
  const customStyles = {
    option:(provided, state) => {
        return {
          ...provided,
          backgroundColor: state.isSelected ? state.data.color.back : '#fff',
          color: state.isSelected ? state.data.color.text : '#000',
        }
    },
    control:(provided, state) => {
      const data = state.getValue()[0];
        return {
          ...provided,
          backgroundColor: data.color.back,
          boxShadow: 'none',
          border: 0,
          borderRadius: '6px',
          zIndex: state.menuIsOpen ? 11 : 1,
        }
    },
    singleValue:(provided, state) => {
      const data = state.getValue()[0];
        return {
          ...provided,
          color: data.color.text,
        }
    },
    dropdownIndicator:(provided, state) => {
      const data = state.getValue()[0];
        return {
          ...provided,
          color: data.color.text,
        }
    },
    menu:(provided, state) => {
        return {
          ...provided,
          borderTop: 'none',
          top: '22px',
          zIndex: 10,
        }
    },
  }
  return (
    <Select
      isSearchable={false}
      className='react_select_container'
      classNamePrefix='react-select-order-status'
      defaultValue={orderStatusOptions[2]}
      onChange={(e) => {}}
      styles={customStyles}
      maxMenuHeight={'100%'}
      options={orderStatusOptions}
    />
  );
};
export default OrderStatusSelect;
