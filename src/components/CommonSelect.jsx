import React from "react";
import Select from "react-select";

const CommonSelect = ({ options, ...props }) => {
  return (
    <Select
      isSearchable={false}
      className='react_select_container'
      classNamePrefix='react-select'
      defaultValue={options[0]}
      // onChange={(e) => {}}
      options={options}
      {...props}
    />
  );
};
export default CommonSelect;