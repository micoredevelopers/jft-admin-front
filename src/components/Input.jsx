import { useField } from "formik";
import React from "react";

const Input = ({ ...props }) => {
  const [field, meta] = useField(props);
  return (
    <div className='input_box_wrapper'>
      {meta.touched && meta.error && (
        <div className='error_message'>{meta.error}</div>
      )}
      <div style={{ position: "relative" }} className='input_box'>
        <input
          className={meta.touched && meta.error ? "input_error" : ""}
          {...field}
          {...props}
        />
      </div>
    </div>
  );
};

export default Input;
