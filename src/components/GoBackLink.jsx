import { arrowBig } from '../assets/img';
import { useHistory } from 'react-router-dom';

const GoBackLink = () => {
	const history = useHistory();
	const handleClick = () => {
		history.goBack();
	}
	return (
		<div onClick={handleClick} className='admin_header_back_btn go_back_btn'>
			<img src={arrowBig} />
			<h3>Назад</h3>
		</div>
	)
}
export default GoBackLink;