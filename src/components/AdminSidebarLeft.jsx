import React, { useState } from "react";
import { arrowBig, clients, cocaCola, cocaColaShort, exit, orders, products, settings, sprite } from '../assets/img';
import {useDispatch} from "react-redux";
import {adminLogout} from "../store/adminSlice";
import {NavLink, Redirect} from "react-router-dom";
import { ReactSVG } from 'react-svg';

const AdminSidebarLeft = ({isAuth}) => {
  const [sidebarActive, setSidebarActive] = useState(false);
  const dispatch = useDispatch();
  if(!isAuth){
    // return <Redirect to='/admin/signin' />
  }
  return (
    <section className={`admin_sidebar_left ${sidebarActive && 'active'}`}>
      <div className='admin_sidevar_left_wrap'>
        <div className='admin_sidebar_left_logo'>
          <ReactSVG src="/img/svg/coca-cola.svg" />
          <ReactSVG className='short' src="/img/svg/coca-cola-short.svg" />
        </div>
        <ul className='admin_sidevar_left_nav'>
          <li>
            <NavLink to='/admin/categories' className='admin_sidebar_left_nav_item'>
              <div className='admin_sidebar_left_nav_icon'>
                <ReactSVG src="/img/svg/products.svg" />
              </div>
              <h2 className='admin_sidebar_left_nav_title'>Товары</h2>
            </NavLink>
          </li>
          <li>
            <NavLink to='/admin/orders' className='admin_sidebar_left_nav_item'>
              <div className='admin_sidebar_left_nav_icon'>
                <ReactSVG src="/img/svg/orders.svg" />
              </div>
              <h2 className='admin_sidebar_left_nav_title'>Заказы</h2>
            </NavLink>
          </li>
          <li>
            <NavLink to='/admin/clients' className='admin_sidebar_left_nav_item'>
              <div className='admin_sidebar_left_nav_icon'>
                <ReactSVG src="/img/svg/clients.svg" />
              </div>
              <h2 className='admin_sidebar_left_nav_title'>Клиенты</h2>
            </NavLink>
          </li>
        </ul>
        <ul className='admin_sidevar_left_options'>
          <li className='admin_sidebar_left_nav_item'>
            <div className='admin_sidebar_left_nav_icon'>
              <ReactSVG src="/img/svg/settings.svg" />
            </div>
            <h2 className='admin_sidebar_left_nav_title'>Настройки</h2>
          </li>
          <li onClick={() => {
            dispatch(adminLogout());
          }} className='admin_sidebar_left_nav_item'>
            <div className='admin_sidebar_left_nav_icon'>
              <ReactSVG src="/img/svg/exit.svg" />
            </div>
            <h2 className='admin_sidebar_left_nav_title'>Выход</h2>
          </li>
        </ul>
        <div onClick={() => setSidebarActive(!sidebarActive)} className='admin-sidebar_left_toggle_btn go_back_btn'>
          <ReactSVG src="/img/svg/arrow-big.svg" />
          <h3>Свернуть</h3>
        </div>
      </div>
    </section>
  );
};
export default AdminSidebarLeft;
