import React from "react";
import Select from "react-select";

const LanguageSelect = ({ ...props }) => {
  return (
    <Select
      isSearchable={false}
      className='react_select_container'
      classNamePrefix='react-select'
      defaultValue={{ value: "RU", label: "RU", type: 1 }}
      options={[
        { value: "RU", label: "RU", type: 1 },
        { value: "UA", label: "UA", type: 1 },
      ]}
      {...props}
    />
  );
};
export default LanguageSelect;
