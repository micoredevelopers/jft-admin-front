import ReactPaginate from 'react-paginate';
import { useHistory, useLocation } from 'react-router-dom';
import {stringify} from "query-string";


const Pagination = ({pageCount, paginationParams}) => {
	const history = useHistory();
	const location = useLocation();
	const route = location.pathname;
	const onPageChange = (e) => {
		history.push(`${route}?${stringify({...paginationParams, page: e.selected + 1})}`);
	}
	const hanldeStartEndButtons = (mode) => () => {
		switch (mode) {
			case 'end':
				history.push(`${route}?${stringify({...paginationParams, page: pageCount})}`);
				break;
			case 'start':
				history.push(`${route}?${stringify({...paginationParams, page: 1})}`);
				break;
			default:
				break;
		}

	};
	return (
		<section className="admin_products_search_pagination_buttons">
			<div onClick={hanldeStartEndButtons('start')} className="admin_products_search_pagination_item start">
				<span>В начало</span>
			</div>
			<ReactPaginate
				forcePage={paginationParams.page ? +paginationParams.page - 1 : 0}
				pageCount={pageCount}
				pageRangeDisplayed={3}
				marginPagesDisplayed={2}
				containerClassName="pagination_container"
				pageClassName="admin_products_search_pagination_item" W
				breakClassName="admin_products_search_pagination_item"
				previousClassName="admin_products_search_pagination_item d-none"
				nextClassName="admin_products_search_pagination_item d-none"
				activeClassName="active"
				onPageChange={onPageChange}
				/>
			<div onClick={hanldeStartEndButtons('end')} className="admin_products_search_pagination_item end">
				<span>В конец</span>
			</div>
		</section>
	);
};
export default Pagination;