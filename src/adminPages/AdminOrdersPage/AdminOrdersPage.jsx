import React, { useEffect } from 'react';
import { NavLink } from "react-router-dom";
import {
  arrowBig,
  bedPreview,
  listIcon,
  plus,
  productColorPreview,
  starActive,
} from "../../assets/img";
import CommonSelect from "../../components/CommonSelect";
import OrderStatusSelect from "./../../components/OrderStatusSelect";
import PaginationSelect from "../../components/PaginationSelect";
import { ReactSVG } from 'react-svg';

const responsibilitySelectOptions = [
  { value: "Ответственный", label: "Ответственный", type: 1 },
  {
    value: "Последние добавления",
    label: "Последние добавления",
    type: 1,
  },
  {
    value: "По цене (сначала дешевые)",
    label: "По цене (сначала дешевые)",
    type: 2,
  },
  {
    value: "По цене (сначала дорогие)",
    label: "По цене (сначала дорогие)",
    type: 4,
  },
  { value: "По названию (а-я)", label: "По названию (а-я)", type: 4 },
  { value: "По названию (я-а)", label: "По названию (я-а)", type: 4 },
  { value: "Черновики", label: "Черновики", type: 4 },
];
const statusSelectOptions = [
  { value: "Новый", label: "Новый", type: 1 },
  { value: "Взял в работу", label: "Взял в работу", type: 1 },
  { value: "В обработке", label: "В обработке", type: 2 },
  { value: "Предзаказ", label: "Предзаказ", type: 4 },
  { value: "На складе", label: "На складе", type: 4 },
  { value: "Доставка", label: "Доставка", type: 4 },
  { value: "Доставка", label: "Доставка", type: 4 },
  { value: "Получено", label: "Получено", type: 4 },
  { value: "Завершен", label: "Завершен", type: 4 },
  { value: "Возврат", label: "Возврат", type: 4 },
];
const typeSelectOptions = [
  { value: "Розница", label: "Розница", type: 1 },
  { value: "Бизнес", label: "Бизнес", type: 2 },
];

const AdminOrdersPage = () => {

  return (
    <section className='admin_main_wrap'>
      <section className='admin_header'>
        <div className='admin_header_tile_wrap'>
          <h1 className='admin_header_title'>Заказы</h1>
          <h2 className='admin_header_subtitle'>Найдено: 3 512 заказов</h2>
        </div>
        <div className='input_box search_input_box'>
          <input className='input_box_input' placeholder='Поиск' />
        </div>
        <div className='admin_search_select'>
          <CommonSelect options={responsibilitySelectOptions} />
        </div>
        <div className='admin_search_select'>
          <CommonSelect options={statusSelectOptions} />
        </div>
        <div className='admin_search_select'>
          <CommonSelect options={typeSelectOptions} />
        </div>
        <button className='admin_header_btn btn_type_2'>
          <span>Создать заказ</span>
          <ReactSVG src="/img/svg/plus.svg" />
        </button>
      </section>
      <section className='admin_main'>
        <section className='admin_main_content'>
          <section className='admin_products_search_results_wrap'>
            <ul className='admin_products_search'>
              <li className='admin_products_search_item title'>
                <div className='admin_orders_search_item_col number'>Номер заказа</div>
                <div className='admin_orders_search_item_col name'>Ответственный</div>
                <div className='admin_orders_search_item_col name'>Фамилия, имя</div>
                <div className='admin_orders_search_item_col phone'>Телефон</div>
                <div className='admin_orders_search_item_col price'>Куплено на</div>
                <div className='admin_orders_search_item_col price'>Оплачено</div>
                <div className='admin_orders_search_item_col status'>Статус</div>
                <div className='admin_orders_search_item_col'></div>
              </li>
              <li className='admin_products_search_item'>
                <div className='admin_orders_search_item_col number'><span>1602</span><span className='date'>19.12.2019</span></div>
                <div className='admin_orders_search_item_col name'>Андреев Андрей</div>
                <div className='admin_orders_search_item_col name orange'>Андреев Андрей</div>
                <div className='admin_orders_search_item_col phone'>+38 (096) 123 45 67</div>
                <div className='admin_orders_search_item_col price'>10 532 ₴</div>
                <div className='admin_orders_search_item_col price'>5 352 ₴</div>
                <div className='admin_orders_search_item_col status'><OrderStatusSelect /></div>
                <div className='admin_orders_search_item_col'>
                  <NavLink
                    to='/admin/product-editing'
                    className='admin_orders_search_item_burger'>
                    <ReactSVG src="/img/svg/list.svg" />
                  </NavLink>
                </div>
              </li>
              <li className='admin_products_search_item'>
                <div className='admin_orders_search_item_col number'><span>1602</span><span className='date'>19.12.2019</span></div>
                <div className='admin_orders_search_item_col name'>Андреев Андрей</div>
                <div className='admin_orders_search_item_col name orange'>Андреев Андрей</div>
                <div className='admin_orders_search_item_col phone'>+38 (096) 123 45 67</div>
                <div className='admin_orders_search_item_col price'>10 532 ₴</div>
                <div className='admin_orders_search_item_col price'>5 352 ₴</div>
                <div className='admin_orders_search_item_col status'><OrderStatusSelect /></div>
                <div className='admin_orders_search_item_col'>
                  <NavLink
                    to='/admin/product-editing'
                    className='admin_orders_search_item_burger'>
                    <ReactSVG src="/img/svg/list.svg" />
                  </NavLink>
                </div>
              </li>
              <li className='admin_products_search_item'>
                <div className='admin_orders_search_item_col number'><span>1602</span><span className='date'>19.12.2019</span></div>
                <div className='admin_orders_search_item_col name'>Андреев Андрей</div>
                <div className='admin_orders_search_item_col name orange'>Андреев Андрей Андреевич</div>
                <div className='admin_orders_search_item_col phone'>+38 (096) 123 45 67</div>
                <div className='admin_orders_search_item_col price'>10 532 ₴</div>
                <div className='admin_orders_search_item_col price'>5 352 ₴</div>
                <div className='admin_orders_search_item_col status'><OrderStatusSelect /></div>
                <div className='admin_orders_search_item_col'>
                  <NavLink
                    to='/admin/product-editing'
                    className='admin_orders_search_item_burger'>
                    <ReactSVG src="/img/svg/list.svg" />
                  </NavLink>
                </div>
              </li>
              <li className='admin_products_search_item'>
                <div className='admin_orders_search_item_col number'><span>1602</span><span className='date'>-</span></div>
                <div className='admin_orders_search_item_col name'>Андреев Андрей</div>
                <div className='admin_orders_search_item_col name orange'>-</div>
                <div className='admin_orders_search_item_col phone'>-</div>
                <div className='admin_orders_search_item_col price'>10 532 ₴</div>
                <div className='admin_orders_search_item_col price'>-</div>
                <div className='admin_orders_search_item_col status'><OrderStatusSelect /></div>
                <div className='admin_orders_search_item_col'>
                  <NavLink
                    to='/admin/product-editing'
                    className='admin_orders_search_item_burger'>
                    <ReactSVG src="/img/svg/list.svg" />
                  </NavLink>
                </div>
              </li>
              
            </ul>
          </section>
          <section className='admin_products_search_pagination'>
            <div className='admin_products_search_pagination_wrap'>
              <div className='admin_products_search_pagination_select-number'>
                <PaginationSelect />
              </div>
              <div className='admin_products_search_pagination_buttons'>
                <button className='admin_products_search_pagination_item start'>
                  В начало
                </button>
                <button className='admin_products_search_pagination_item'>
                  1
                </button>
                <button className='admin_products_search_pagination_item'>
                  2
                </button>
                <button className='admin_products_search_pagination_item'>
                  3
                </button>
                <button className='admin_products_search_pagination_item'>
                  ...
                </button>
                <button className='admin_products_search_pagination_item'>
                  1103
                </button>
              </div>
            </div>
          </section>
        </section>
        <section className='admin_sidebar_right admin_orders_search_sidebar search-page active'>
          <div className='admin_orders_search_sidebar_item header'>
            <div className='admin_orders_search_sidebar_item_left-col'>
              <div className='admin_orders_search_sidebar_header_select'>
                <OrderStatusSelect />
              </div>
            </div>
            <div className='admin_orders_search_sidebar_item_right-col '>
              <div className='admin_orders_search_sidebar_header_time'>
                19.12.2019, в 13:30
              </div>
            </div>
          </div>
          <div className='admin_orders_search_sidebar_item'>
            <div className='admin_orders_search_sidebar_item_left-col'>
              <div className="admin_orders_search_sidebar_order">
                <div className="admin_orders_search_sidebar_title">Заказ</div>
                <div className="admin_orders_search_sidebar_order_price"><span className="black">22 105,00 ₴</span></div>
                <div className="admin_orders_search_sidebar_order_price"><span className="green">22 105,00 ₴</span><span className="red">0 ₴</span></div>
              </div>
            </div>
            <div className='admin_orders_search_sidebar_item_right-col'>
              <div className="admin_orders_search_sidebar_order_number">
                №1602
              </div>
            </div>
          </div>
          <div className='admin_orders_search_sidebar_item'>
            <div className='admin_orders_search_sidebar_item_left-col'>
              <div className="admin_orders_search_sidebar_order">
                <div className="admin_orders_search_sidebar_title">Пользователь</div>
                <div className="admin_orders_search_sidebar_name">Андреев Андрей</div>
                <div className="admin_orders_search_sidebar_text">Бизнес аккаунт</div>
                <div className="admin_orders_search_sidebar_text">+38 (096) 123 45 67</div>
              </div>
            </div>
          </div>
          <div className='admin_orders_search_sidebar_item'>
            <div className='admin_orders_search_sidebar_item_left-col'>
              <div className="admin_orders_search_sidebar_order">
                <div className="admin_orders_search_sidebar_title">Доставка</div>
                <div className="admin_orders_search_sidebar_text">Андреев Андрей Андреевич</div>
                <div className="admin_orders_search_sidebar_text">Наложенный платёж</div>
                <div className="admin_orders_search_sidebar_text">Одесса, 7 отделение Новой Почты</div>
              </div>
            </div>
          </div>
          <div className='admin_orders_search_sidebar_item'>
            <div className='admin_orders_search_sidebar_item_left-col'>
              <div className="admin_orders_search_sidebar_order">
                  <div className="admin_orders_search_sidebar_title">Комментарий</div>
                  <div className="admin_orders_search_sidebar_text">Наберёте плз для уточнения деталей</div>
              </div>
            </div>
          </div>
          <div className="admin_orders_search_sidebar_products">
            <div className="admin_orders_search_sidebar_title">Товары</div>
            <div className="admin_orders_search_sidebar_products_wrap">
              <div className="admin_orders_search_sidebar_products_item">
                <div className="admin_orders_search_sidebar_products_item_img">
                  <img src={bedPreview} alt="" />
                </div>
                <div className="admin_orders_search_sidebar_products_item_info">
                  <div className="admin_orders_search_sidebar_products_item_suptitle">
                    <span>15259692</span>
                    <span>Синий цвет</span>
                  </div>
                  <div className="admin_orders_search_sidebar_products_item_title">Кровать софия люкс с механизмом</div>
                  <div className="admin_orders_search_sidebar_products_item_price">
                    <span className="price">1 500 ₴</span>
                    <span className="amount">5 шт</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </section>
    </section>
  );
};
export default AdminOrdersPage;
