import React, {useState} from "react";
import CommonSelect from "../../components/CommonSelect";
import {arrowBig, bedPreview, list, plus} from "../../assets/img";
import OrderStatusSelect from "../../components/OrderStatusSelect";
import {NavLink} from "react-router-dom";
import PaginationSelect from "../../components/PaginationSelect";

const CurrentClient = () => {
		const [clientInfoBlock, setClientIfoBlock] = useState("orders");
		const onOpenOrdersInfoClick = () => {
				setClientIfoBlock("orders");
		};
		const onOpenPaymentsInfoClick = () => {
				setClientIfoBlock("payments");
		};
		return (
			<section className="admin_main_wrap">
					<section className="admin_header">
							<NavLink to="/admin/clients" className="admin_header_back_btn go_back_btn small">
									<img src={arrowBig}/>
									<h3>Назад</h3>
							</NavLink>
							<h1 className="admin_header_title">Андреев Андрей</h1>
					</section>
					<section className="admin_main">
							<section className="admin_main_content">
									<section className="about_client">
											<div className="about_client_title">О клиенте</div>
											<section className="admin_products_search_results_wrap">
													<ul className="admin_products_search">
															<li className="admin_products_search_item title">
																	<div className="admin_client_about_client_item name">
																			Отчество
																	</div>
																	<div className="admin_client_about_client_item phone">
																			Телефон
																	</div>
																	<div className="admin_client_about_client_item mail">
																			Почта
																	</div>
																	<div className="admin_client_about_client_item orders">
																			Покупок
																	</div>
															</li>
															<li className="admin_products_search_item">
																	<div className="admin_client_about_client_item name">
																			Не указано
																	</div>
																	<div className="admin_client_about_client_item phone">
																			+38 096 554 91 27
																	</div>
																	<div className="admin_client_about_client_item mail">
																			mail@gmail.com
																	</div>
																	<div className="admin_client_about_client_item orders">
																			1032
																	</div>
															</li>
													</ul>
											</section>
									</section>
									<section className="client_info">
											<div className="clients_info_header">
													<div className="navigation_btns">
															<div className={`navigation_btn_item ${clientInfoBlock === "orders" && "active"}`}
																				onClick={onOpenOrdersInfoClick}>Заказы
															</div>
															<div className={`navigation_btn_item ${clientInfoBlock === "payments" && "active"}`}
																				onClick={onOpenPaymentsInfoClick}>Взаиморасчет
															</div>
													</div>
											</div>
											{clientInfoBlock === "orders" && <section className="admin_products_search_results_wrap">
													<ul className="admin_products_search">
															<li className="admin_products_search_item no-padding no-border title">
																	<div className="admin_orders_info_item all">
																			Всего
																	</div>
																	<div className="admin_orders_info_item refuses">
																			Отказы
																	</div>
																	<div className="admin_orders_info_item bought">
																			Покупок
																	</div>
															</li>
															<li className="admin_products_search_item no-border bold">
																	<div className="admin_orders_info_item all">
																			2 292 745.00 ₴
																	</div>
																	<div className="admin_orders_info_item refuses">
																			2 745.00 ₴
																	</div>
																	<div className="admin_orders_info_item bought">
																			1 032
																	</div>
															</li>
															<li className="admin_products_search_item title">
																	<div className="admin_orders_info_item order">
																			Заказ
																	</div>
																	<div className="admin_orders_info_item date">Дата</div>
																	<div className="admin_orders_info_item status">
																			Статус
																	</div>
																	<div className="admin_orders_info_item products">
																			Товары
																	</div>
																	<div className="admin_orders_info_item summ">
																			Сумма
																	</div>
															</li>
															<li className="admin_products_search_item">
																	<div className="admin_orders_info_item order bold">
																			1602
																	</div>
																	<div className="admin_orders_info_item date">
																			19.12.2019
																	</div>
																	<div className="admin_orders_info_item status">
																			<div className="status_wrap">
																					<OrderStatusSelect/>
																			</div>
																	</div>
																	<div className="admin_orders_info_item products">
																			Престиж-1 120*200 - 6 шт
																	</div>
																	<div className="admin_orders_info_item summ bold">
																			15 726,00
																	</div>
															</li>
															<li className="admin_products_search_item">
																	<div className="admin_orders_info_item order bold">
																			1602
																	</div>
																	<div className="admin_orders_info_item date">
																			19.12.2019
																	</div>
																	<div className="admin_orders_info_item status">
																			<div className="status_wrap">
																					<OrderStatusSelect/>
																			</div>
																	</div>
																	<div className="admin_orders_info_item products">
																			Престиж-1 120*200 - 6 шт
																	</div>
																	<div className="admin_orders_info_item summ bold">
																			15 726,00
																	</div>
															</li>
															<li className="admin_products_search_item">
																	<div className="admin_orders_info_item order bold">
																			1602
																	</div>
																	<div className="admin_orders_info_item date">
																			19.12.2019
																	</div>
																	<div className="admin_orders_info_item status">
																			<div className="status_wrap">
																					<OrderStatusSelect/>
																			</div>
																	</div>
																	<div className="admin_orders_info_item products">
																			Престиж-1 120*200 - 6 шт
																	</div>
																	<div className="admin_orders_info_item summ bold">
																			15 726,00
																	</div>
															</li>
													</ul>
											</section>}
											{clientInfoBlock === "payments" && <section className="admin_products_search_results_wrap">
													<ul className="admin_products_search">
															<li className="admin_products_search_item no-padding no-border title">
																	<div className="admin_orders_info_item all">
																			Всего
																	</div>
																	<div className="admin_orders_info_item all-products">
																			Товаров
																	</div>
																	<div className="admin_orders_info_item paid">
																			Выплачено
																	</div>
																	<div className="admin_orders_info_item arrear">
																			Долг
																	</div>
															</li>
															<li className="admin_products_search_item bold no-padding no-border">
																	<div className="admin_orders_info_item all">
																			2 292 745.00 ₴
																	</div>
																	<div className="admin_orders_info_item all-products">
																			1 032
																	</div>
																	<div className="admin_orders_info_item green paid">
																			2 124 206,00
																	</div>
																	<div className="admin_orders_info_item red arrear">
																			168 539,00
																	</div>
															</li>
															<li className="admin_products_search_item title">
																	<div className="admin_orders_info_item order">
																			Заказ
																	</div>
																	<div className="admin_orders_info_item date">Дата</div>
																	<div className="admin_orders_info_item status">
																			Статус
																	</div>
																	<div className="admin_orders_info_item products">
																			Товары
																	</div>
																	<div className="admin_orders_info_item summ">
																			Сумма
																	</div>
															</li>
													</ul>
											</section>}
									</section>

							</section>
							<section className="admin_sidebar_right active">
									<div className="admin_sidebar_right_client_wrap active"> {/* add active to show order*/}
											<div className="admin_sidebar_right_client">
													<div className="admin_orders_search_sidebar_item header">
															<div className="admin_orders_search_sidebar_item_left-col">
																	<div className="admin_orders_search_sidebar_header_select">
																			<OrderStatusSelect/>
																	</div>
															</div>
															<div className="admin_orders_search_sidebar_item_right-col ">
																	<div className="admin_orders_search_sidebar_header_time">
																			19.12.2019, в 13:30
																	</div>
															</div>
													</div>
													<div className="admin_orders_search_sidebar_item">
															<div className="admin_orders_search_sidebar_item_left-col">
																	<div className="admin_orders_search_sidebar_order">
																			<div className="admin_orders_search_sidebar_title">Заказ</div>
																			<div className="admin_orders_search_sidebar_order_price"><span className="black">22 105,00 ₴</span>
																			</div>
																			<div className="admin_orders_search_sidebar_order_price"><span
																				className="green">22 105,00 ₴</span><span className="red">0 ₴</span></div>
																	</div>
															</div>
															<div className="admin_orders_search_sidebar_item_right-col">
																	<div className="admin_orders_search_sidebar_order_number">
																			№1602
																	</div>
															</div>
													</div>
													<div className="admin_orders_search_sidebar_item">
															<div className="admin_orders_search_sidebar_item_left-col">
																	<div className="admin_orders_search_sidebar_order">
																			<div className="admin_orders_search_sidebar_title">Пользователь</div>
																			<div className="admin_orders_search_sidebar_name">Андреев Андрей</div>
																			<div className="admin_orders_search_sidebar_text">Бизнес аккаунт</div>
																			<div className="admin_orders_search_sidebar_text">+38 (096) 123 45 67</div>
																	</div>
															</div>
													</div>
													<div className="admin_orders_search_sidebar_item">
															<div className="admin_orders_search_sidebar_item_left-col">
																	<div className="admin_orders_search_sidebar_order">
																			<div className="admin_orders_search_sidebar_title">Доставка</div>
																			<div className="admin_orders_search_sidebar_text">Андреев Андрей Андреевич</div>
																			<div className="admin_orders_search_sidebar_text">Наложенный платёж</div>
																			<div className="admin_orders_search_sidebar_text">Одесса, 7 отделение Новой Почты</div>
																	</div>
															</div>
													</div>
													<div className="admin_orders_search_sidebar_item">
															<div className="admin_orders_search_sidebar_item_left-col">
																	<div className="admin_orders_search_sidebar_order">
																			<div className="admin_orders_search_sidebar_title">Комментарий</div>
																			<div className="admin_orders_search_sidebar_text">Наберёте плз для уточнения деталей</div>
																	</div>
															</div>
													</div>
													<div className="admin_orders_search_sidebar_products">
															<div className="admin_orders_search_sidebar_title">Товары</div>
															<div className="admin_orders_search_sidebar_products_wrap">
																	<div className="admin_orders_search_sidebar_products_item">
																			<div className="admin_orders_search_sidebar_products_item_img">
																					<img src={bedPreview} alt=""/>
																			</div>
																			<div className="admin_orders_search_sidebar_products_item_info">
																					<div className="admin_orders_search_sidebar_products_item_suptitle">
																							<span>15259692</span>
																							<span>Синий цвет</span>
																					</div>
																					<div className="admin_orders_search_sidebar_products_item_title">Кровать софия люкс с механизмом
																					</div>
																					<div className="admin_orders_search_sidebar_products_item_price">
																							<span className="price">1 500 ₴</span>
																							<span className="amount">5 шт</span>
																					</div>
																			</div>
																	</div>
															</div>
													</div>
											</div>
											<div className="admin_sidebar_right_client_unactive-message">Выберите заказ для просмотра</div>
									</div>
							</section>
					</section>
			</section>
		);
};
export default CurrentClient;