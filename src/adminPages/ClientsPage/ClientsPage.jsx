import React from "react";
import {NavLink} from "react-router-dom";
import {listIcon, plus} from "../../assets/img";
import CommonSelect from "../../components/CommonSelect";
import PaginationSelect from "../../components/PaginationSelect";

const filterSelectOptions = [
		{value: "По фамилии (а-я)", label: "По фамилии (а-я)", type: 1},
		{
				value: "По фамилии (я-а)",
				label: "По фамилии (я-а)",
				type: 1,
		},
		{
				value: "Отрицательный баланс",
				label: "Отрицательный баланс",
				type: 2,
		},
		{
				value: "Положительный баланс",
				label: "Положительный баланс",
				type: 4,
		},
];

const ClientsPage = () => {
		return (
			<section className='admin_main_wrap'>
					<section className='admin_header'>
							<div className='admin_header_tile_wrap'>
									<h1 className='admin_header_title'>Клиенты</h1>
									<h2 className='admin_header_subtitle'>Найдено: 1 242 клиента</h2>
							</div>
							<div className='input_box search_input_box'>
									<input className='input_box_input' placeholder='Поиск'/>
							</div>
							<div className='admin_search_select'>
									<CommonSelect options={filterSelectOptions}/>
							</div>
							<div className='admin_search_select'>
									<CommonSelect options={filterSelectOptions}/>
							</div>
					</section>
					<section className='admin_main'>
							<section className='admin_main_content'>
									<section className='admin_products_search_results_wrap'>
											<ul className='admin_products_search'>
													<li className='admin_products_search_item title'>
															<div className='admin_clients_search_item_col name'>
																	Фамилия, имя
															</div>
															<div className='admin_clients_search_item_col orders'>
																	Заказов
															</div>
															<div className='admin_clients_search_item_col'>
																	Куплено на
															</div>
															<div className='admin_clients_search_item_col'>
																	Баланс
															</div>
															<div className='admin_clients_search_item_col phone'>
																	Телефон
															</div>
															<div className='admin_clients_search_item_col mail'>
																	Почта
															</div>
															<div className='admin_clients_search_item_col burger'></div>
													</li>
													<li className='admin_products_search_item'>
															<div className='admin_clients_search_item_col name'>
																	Андреев Андрей
															</div>
															<div className='admin_clients_search_item_col orders'>
																	4
															</div>
															<div className='admin_clients_search_item_col'>
																	10 532 ₴
															</div>
															<div className='admin_clients_search_item_col green'>
																	+10 532 ₴
															</div>
															<div className='admin_clients_search_item_col phone'>
																	+38 (096) 123 45 67
															</div>
															<div className='admin_clients_search_item_col mail'>
																	mail@gmail.com
															</div>
															<div className='admin_clients_search_item_col  burger'>
																	<NavLink
																		to={`/admin/clients/${1}`}
																		className='admin_orders_search_item_burger'>
																			<img src={listIcon}/>
																	</NavLink>
															</div>
													</li>
													<li className='admin_products_search_item'>
															<div className='admin_clients_search_item_col name'>
																	Андреев Андрей
															</div>
															<div className='admin_clients_search_item_col orders'>
																	4
															</div>
															<div className='admin_clients_search_item_col'>
																	10 532 ₴
															</div>
															<div className='admin_clients_search_item_col red'>
																	-2 342 ₴
															</div>
															<div className='admin_clients_search_item_col phone'>
																	+38 (096) 123 45 67
															</div>
															<div className='admin_clients_search_item_col mail'>
																	mail@gmail.com
															</div>
															<div className='admin_clients_search_item_col  burger'>
																	<NavLink
																		to={`/admin/clients/${1}`}
																		className='admin_orders_search_item_burger'>
																			<img src={listIcon}/>
																	</NavLink>
															</div>
													</li>

											</ul>
									</section>
									<section className='admin_products_search_pagination'>
											<div className='admin_products_search_pagination_wrap'>
													<div className='admin_products_search_pagination_select-number'>
															<PaginationSelect/>
													</div>
													<div className='admin_products_search_pagination_buttons'>
															<button className='admin_products_search_pagination_item start'>
																	В начало
															</button>
															<button className='admin_products_search_pagination_item'>
																	1
															</button>
															<button className='admin_products_search_pagination_item'>
																	2
															</button>
															<button className='admin_products_search_pagination_item'>
																	3
															</button>
															<button className='admin_products_search_pagination_item'>
																	...
															</button>
															<button className='admin_products_search_pagination_item'>
																	1103
															</button>
													</div>
											</div>
									</section>
							</section>
					</section>
			</section>
		);
};
export default ClientsPage;
