import React, { useEffect } from 'react';
import { bedPreview } from '../../assets/img';
import Select from "react-select";
import { useHistory, useLocation, useParams } from 'react-router-dom';
import CommonSelect from '../../components/CommonSelect';
import { useDispatch, useSelector } from 'react-redux';
import { getCategoryProducts } from '../../store/categories';
import { parse, stringify } from "query-string";
import { ReactSVG } from 'react-svg';
import { updateProductReducer } from '../../store/productsSlice';
import LimitPagination from '../../components/LimitPagination';
import Pagination from '../../components/Pagination';

const searchSelectOptions = [
	{ value: "Последние добавления", label: "Последние добавления", type: 1 },
	{ value: "По названию (а-я)", label: "По названию (а-я)", type: 2 },
	{ value: "По названию (я-а)", label: "По названию (я-а)", type: 3 },
	// { value: "По цене (сначала дешевые)", label: "По цене (сначала дешевые)", type: 4 },
	// { value: "По цене (сначала дорогие)", label: "По цене (сначала дорогие)", type: 5 },
	// { value: "Черновики", label: "Черновики", type: 7 },
]

const CategoryList = () => {
	const dispatch = useDispatch();
	const history = useHistory();
	const location = useLocation();
	const {id: categoryId} = useParams();
	const {products} = useSelector(state => state.categories);
	const pageCount = Math.ceil(products.total / +products.per_page);
	const params = parse(location.search);
	const {page, search, per_page, sort, direction} = params;
	const handleProductClick = (id) => () => {
		history.push(`/admin/product-editing/${id}`)
	}
	const updateProduct = (id, status) => () => {
		dispatch(updateProductReducer(id, status)).then(() => {
			dispatch(getCategoryProducts(categoryId, params));
		});
	}
	const onSearch = (e) => {
		history.push(`${location.pathname}?${stringify({...params, page: 1, search: e.target.value})}`);
	}
	const onLimitChange = (e) => {
		history.push(`${location.pathname}?${stringify({...params, page: 1, per_page: e.value})}`);
	}
	const onSortSelectChange = (e) => {
		switch (e.type){
			case 1:
				history.push(`${location.pathname}?${stringify({...params, sort: 'created_at', direction: 'asc'})}`);
				break;
			case 2:
				history.push(`${location.pathname}?${stringify({...params, sort: 'name', direction: 'asc'})}`);
				break;
			case 3:
				history.push(`${location.pathname}?${stringify({...params, sort: 'name', direction: 'desc'})}`);
				break;
			case 4:
				history.push(`${location.pathname}?${stringify({...params, sort: 'name', direction: 'asc'})}`);
				break;
			case 5:
				history.push(`${location.pathname}?${stringify({...params, sort: 'name', direction: 'desc'})}`);
				break;
			case 6:
				history.push(`${location.pathname}?${stringify({...params, sort: '', direction: ''})}`);
				break;
			default: break;
		}
	}
	useEffect(() => {
		dispatch(getCategoryProducts(categoryId, params));
	}, [page, search, per_page, sort, direction])
	return (
		<section className='admin_main_wrap'>
			<section className='admin_header'>
				<div className='admin_header_tile_wrap'>
					<h1 className='admin_header_title'>Товары</h1>
					<h2 className='admin_header_subtitle'>Найдено: 3 512 заказов</h2>
				</div>
				<div className='input_box search_input_box'>
					<input onChange={onSearch} className='input_box_input' placeholder='Поиск' />
				</div>
				<div className="admin_search_select">
					<CommonSelect onChange={onSortSelectChange} options={searchSelectOptions} />
				</div>
			</section>
			<section className='admin_main'>
				<section className='admin_main_content'>
					<section className='table_wrap'>
						<table>
							<thead>
							<tr className="table_head">
								<th className="table_item">Фото</th>
								<th className="table_item">Название</th>
								<th className="table_item">ID товара</th>
								<th className="table_item">Артикул</th>
								<th className="table_item">Стоимость</th>
								<th className="table_item">Скидка</th>
								<th className="table_item">Наличие</th>
								<th className="table_item">Под заказ</th>
								<th className="table_item">Статус</th>
								<th className="table_item"></th>
							</tr>
							</thead>
							<tbody>
							{!!products.data &&
							products.data.map((e, i) => (
								<tr key={i} className="table-row">
									<td className="table_item">
										<div className="table-categories_img">
											<img src={bedPreview} alt=""/>
										</div>
									</td>
									<td className="table_item product_name">{e.name}</td>
									<td className="table_item">{e.id}</td>
									<td className="table_item">{e.article}</td>
									<td className="table_item">{+e.amount} ₴</td>
									<td className="table_item">0 ₴</td>
									<td className="table_item">1 шт</td>
									<td className="table_item">21 день</td>
									<td className="table_item">
										{e.active === 1 ?
											<div className="category_status">На сайте</div> :
											<div className="category_status disabled">Отключён</div>}
									</td>
									<td className="table_item">
										<div className="table-burger category_burger">
											<ReactSVG src="/img/svg/list.svg"/>
											<div className="category_list_popup_wrap">
												<div className="category_list_popup">
													<div onClick={handleProductClick(e.id)}  className="category_list_popup_item">Редактировать</div>
													<div  className="category_list_popup_item">Открыть на сайте</div>
													{e.active === 1 ?
														<div onClick={updateProduct(e.id, 0)} className="category_list_popup_item red">Отключить</div> :
														<div onClick={updateProduct(e.id, 1)} className="category_list_popup_item">Включить</div>
													}
												</div>
											</div>
										</div>
									</td>
								</tr>
							))
							}
							</tbody>
						</table>
					</section>
					<section className='admin_products_search_pagination'>
						<div className='admin_products_search_pagination_wrap'>
							<div className='admin_products_search_pagination_select-number'>
								<LimitPagination
									onChange={onLimitChange}
									defaultValue={per_page}
								/>
							</div>
							{!!products.data &&
							<Pagination pageCount={pageCount} paginationParams={params} />
							}
						</div>
					</section>
				</section>
				{/*<section className={`admin_sidebar_right search-page ${sidebarActive && 'active'}`}>*/}
				{/*	<div className='admin_sidebar_product_preview'>*/}
				{/*		<h2 className='admin_sidebar_product_preview_title'>Превью товара</h2>*/}
				{/*		<div className='admin_sidebar_product_preview_wrap'>*/}
				{/*			<div className='admin_sidebar_product_preview_img'>*/}
				{/*				<img src={bedPreview} />*/}
				{/*			</div>*/}
				{/*			<div className='admin_sidebar_product_preview_info'>*/}
				{/*				<div className='admin_sidevar_product_preview_reviews'>*/}
				{/*					<div className='admin_sidebar_product_preview_stars'>*/}
				{/*						<img src={starActive} />*/}
				{/*						<img src={starActive} />*/}
				{/*						<img src={starActive} />*/}
				{/*						<img src={starActive} />*/}
				{/*						<img src={starActive} />*/}
				{/*						<span className='stars_count'>5</span>*/}
				{/*						<span className='reviews_count'>(9 отзывов)</span>*/}
				{/*					</div>*/}
				{/*					<h2 className='admin_sidebar_product_preview_article'>*/}
				{/*						<span>Артикул: 15259692</span>*/}
				{/*						<span className='count'>1 500 товаров</span>*/}
				{/*					</h2>*/}
				{/*					<h1 className='admin_sidebar_product_preview_title'>*/}
				{/*						Кровать софия люкс с механизмом*/}
				{/*					</h1>*/}
				{/*					<div className='asmin_sidebar_product_preview_price'>*/}
				{/*						<span className='price_new'>1 500 ₴</span>*/}
				{/*						<span className='price_old'>2 000 ₴</span>*/}
				{/*						<span className='stock'>В наличии</span>*/}
				{/*					</div>*/}
				{/*				</div>*/}
				{/*			</div>*/}
				{/*		</div>*/}
				{/*	</div>*/}
				{/*	<ul className='admin_sidebar_right_colors'>*/}
				{/*		<li className='admin_sidebar_color_item'>*/}
				{/*			<div className='admin_sidebar_color_preview-img'>*/}
				{/*				<img src={productColorPreview} />*/}
				{/*			</div>*/}
				{/*			<div className='admin_sidevar_color_info'>*/}
				{/*				<h1 className='admin_sidebar_color_info_title'>*/}
				{/*					Дуб Ансберг Темный*/}
				{/*				</h1>*/}
				{/*				<div className='admin_sidebar_color_info_price'>*/}
				{/*					<span className='price_new'>1 500 ₴</span>*/}
				{/*					<span className='price_old'>2 000 ₴</span>*/}
				{/*				</div>*/}
				{/*				<div className='admin_sidebar_color_info_count'>*/}
				{/*					<span className='title'>В наличии:</span>*/}
				{/*					<span>2 товара</span>*/}
				{/*				</div>*/}
				{/*				<div className='admin_sidebar_color_info_count'>*/}
				{/*					<span className='title'>Под заказ:</span>*/}
				{/*					<span>21 день</span>*/}
				{/*				</div>*/}
				{/*			</div>*/}
				{/*		</li>*/}
				{/*		<li className='admin_sidebar_color_item'>*/}
				{/*			<div className='admin_sidebar_color_preview-img'>*/}
				{/*				<img src={productColorPreview} />*/}
				{/*			</div>*/}
				{/*			<div className='admin_sidevar_color_info'>*/}
				{/*				<h1 className='admin_sidebar_color_info_title'>*/}
				{/*					Дуб Ансберг Темный*/}
				{/*				</h1>*/}
				{/*				<div className='admin_sidebar_color_info_price'>*/}
				{/*					<span className='price_new'>1 500 ₴</span>*/}
				{/*					<span className='price_old'>2 000 ₴</span>*/}
				{/*				</div>*/}
				{/*				<div className='admin_sidebar_color_info_count'>*/}
				{/*					<span className='title'>В наличии:</span>*/}
				{/*					<span>2 товара</span>*/}
				{/*				</div>*/}
				{/*				<div className='admin_sidebar_color_info_count'>*/}
				{/*					<span className='title'>Под заказ:</span>*/}
				{/*					<span>21 день</span>*/}
				{/*				</div>*/}
				{/*			</div>*/}
				{/*		</li>*/}
				{/*	</ul>*/}
				{/*</section>*/}
			</section>
		</section>
	)
}
export default CategoryList;