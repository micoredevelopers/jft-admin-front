import React, { useEffect } from 'react';
import { bedPreview } from '../../assets/img';
import { useHistory, useLocation } from 'react-router-dom';
import CommonSelect from '../../components/CommonSelect';
import { useDispatch, useSelector } from 'react-redux';
import { categoriesListReducer, updateCategoryReducer } from '../../store/categories';
import { ReactSVG } from 'react-svg';
import Pagination from '../../components/Pagination';
import { parse, stringify } from "query-string";
import LimitPagination from '../../components/LimitPagination';
const searchSelectOptions = [
	{ value: "Последние добавления", label: "Последние добавления", type: 1 },
	{ value: "По названию (а-я)", label: "По названию (а-я)", type: 2 },
	{ value: "По названию (я-а)", label: "По названию (я-а)", type: 3 },
	// { value: "Черновики", label: "Черновики", type: 4 },
]

const Categories = () => {
	const dispatch = useDispatch();
	const history = useHistory();
	const location = useLocation();
	const {categoriesList} = useSelector(state => state.categories);
	const pageCount = Math.ceil(categoriesList.total / +categoriesList.per_page);
	const params = parse(location.search);
	const {page, search, per_page, sort, direction} = params;
	const handleCategoryClick = (id) => () => {
		history.push('/admin/category/' + id);
	}
	const updateCategory = (id, status) => () => {
		dispatch(updateCategoryReducer(id, status)).then(() => {
			dispatch(categoriesListReducer(params));
		});
	}
	const onSearch = (e) => {
		history.push(`${location.pathname}?${stringify({...params, page: 1, search: e.target.value})}`);
	}
	const onLimitChange = (e) => {
		history.push(`${location.pathname}?${stringify({...params, page: 1, per_page: e.value})}`);
	}
	const onSortSelectChange = (e) => {
		switch (e.type){
			case 1:
				history.push(`${location.pathname}?${stringify({...params, sort: 'created_at', direction: 'asc'})}`);
				break;
			case 2:
				history.push(`${location.pathname}?${stringify({...params, sort: 'name', direction: 'asc'})}`);
				break;
			case 3:
				history.push(`${location.pathname}?${stringify({...params, sort: 'name', direction: 'desc'})}`);
				break;
			case 4:
				history.push(`${location.pathname}?${stringify({...params, sort: '', direction: ''})}`);
				break;
			default: break;
		}
	}
	useEffect(() => {
		dispatch(categoriesListReducer(params));
	}, [page, search, per_page, sort, direction])
	return (
		<section className='admin_main_wrap'>
			<section className='admin_header'>
				<div className='admin_header_tile_wrap'>
					<h1 className='admin_header_title'>Категории</h1>
					<h2 className='admin_header_subtitle'>Найдено: {categoriesList.total} категорий</h2>
				</div>
				<div className='input_box search_input_box'>
					<input onChange={onSearch} className='input_box_input' placeholder='Поиск' />
				</div>
				<div className="admin_search_select">
					<CommonSelect onChange={onSortSelectChange} options={searchSelectOptions} />
				</div>
			</section>
			<section className='admin_main'>
				<section className='admin_main_content'>
					<section className='table_wrap'>
						<table>
							<thead>
								<tr className="table_head">
									<th className="table_item">Фото</th>
									<th className="table_item">Название</th>
									<th className="table_item">Товаров</th>
									<th className="table_item">Статус</th>
									<th className="table_item"></th>
								</tr>
							</thead>
							<tbody>
							{!!categoriesList.data &&
								categoriesList.data.map((e, i) => (
									<tr key={i} className="table-row">
										<td className="table_item">
											<div className="table-categories_img">
												<img src={bedPreview} alt=""/>
											</div>
										</td>
										<td className="table_item category_name">{e.name}</td>
										<td className="table_item">125</td>
										<td className="table_item">
											{e.active === 1 ?
												<div className="category_status">На сайте</div> :
												<div className="category_status disabled">Отключён</div>}
										</td>
										<td className="table_item">
											<div className="table-burger category_burger">
												<ReactSVG src="/img/svg/list.svg"/>
												<div className="category_list_popup_wrap">
													<div className="category_list_popup">
														<div onClick={handleCategoryClick(e.id)}  className="category_list_popup_item">Перейти</div>
														{e.active === 1 ?
															<div onClick={updateCategory(e.id,0)} className="category_list_popup_item red">Отключить</div> :
															<div onClick={updateCategory(e.id,1)} className="category_list_popup_item">Включить</div>
														}
													</div>
												</div>
											</div>
										</td>
									</tr>
								))
							}
							</tbody>
						</table>
					</section>
					<section className='admin_products_search_pagination'>
						<div className='admin_products_search_pagination_wrap'>
							<div className='admin_products_search_pagination_select-number'>
								<LimitPagination
									onChange={onLimitChange}
									defaultValue={per_page}
								/>
							</div>
							{!!categoriesList.data &&
								<Pagination pageCount={pageCount} paginationParams={params} />
							}
						</div>
					</section>
				</section>
			</section>
		</section>
	)
}
export default Categories;