import React from 'react';
import {eye} from "../../assets/img";
import {useDispatch, useSelector} from "react-redux";
import {adminSetValue, adminSignin, setAdminSigninErrorMessage} from "../../store/adminSlice";
import {Field, Form, Formik} from "formik";
import {Redirect} from "react-router-dom";

const AdminSigninPage = ({isAuth}) => {
    const dispatch = useDispatch();
    const errorMessage = useSelector(state => state.admin.adminSigninErrorMessage);
    return <Redirect to='/admin/search' />
    // if(isAuth){
    //     return <Redirect to='/admin/search' />
    // }
    return (
        <div className='admin-signin'>
            <div className="admin-signin_wrap">
                <h1 className="admin-signin_title">
                    Вход в аккаунт
                </h1>
                <Formik
                    initialValues={{login: '', password: ''}}
                    onSubmit={(values) => {
                        dispatch(adminSignin(values))
                    }}>
                    {() => (
                        <Form onChange={() => {
                            dispatch(setAdminSigninErrorMessage());
                        }}>
                            <div className='input_box admin-signin_input_box'>
                                <Field
                                    type='text'
                                    name='login'
                                    className='input_box_input'
                                    placeholder='Логин'
                                />
                            </div>
                            <div className='input_box admin-signin_input_box'>
                                <Field
                                    name='password'
                                    type='password'
                                    className='input_box_input'
                                    placeholder='********'
                                />
                                <img src={eye} alt="" className="admin-signin_input_box_img"/>
                            </div>
                            <button type='submit' className="admin-signin_btn btn_type_1">Войти</button>
                        </Form>)}
                </Formik>
                {!!errorMessage &&
                <div className="error_message">
                    {errorMessage}
                </div>
                }
            </div>
        </div>
    )
}
export default AdminSigninPage;