import React, { useEffect, useState } from 'react';
import TextEditor from "../../components/TextEditor";
import {  useParams } from 'react-router-dom';
import EditColor from "./EditColor";
import EditParams from "./EditParams";
import EditProduct from "./EditProduct";
import { useDispatch, useSelector } from 'react-redux';
import { showProductReducer } from '../../store/productsSlice';
import GoBackLink from '../../components/GoBackLink';

const TrialSelectOptions = [
{
  value: "Последние добавления",
  label: "Последние добавления",
  type: 1,
},
{
  value: "По цене (сначала дешевые)",
  label: "По цене (сначала дешевые)",
  type: 2,
},
{
  value: "По цене (сначала дорогие)",
  label: "По цене (сначала дорогие)",
  type: 4,
},
{
  value: "По названию (а-я)",
  label: "По названию (а-я)",
  type: 4,
},
{
  value: "По названию (я-а)",
  label: "По названию (я-а)",
  type: 4,
},
{ value: "Черновики", label: "Черновики", type: 4 },
]


const AdminProductEditingPage = () => {
  const dispatch = useDispatch();
  const params = useParams();
  const currentProduct = useSelector(state => state.products.currentProduct);
  const [file, setFile] = useState("");
  useEffect(() => {
    dispatch(showProductReducer(params.id));
  }, []);

  return (
    <section className='admin_main_wrap'>
      <section className='admin_header'>
        <GoBackLink />
        <h1 className='admin_header_title'>Редактирование товара</h1>
        <button className='admin_header_btn btn_type_1'>Редактировать товар</button>
      </section>
      <section className='admin_main'>
        <section className='admin_main_content'>
          <div className='admin_product_editing'>
            {/*<div className='admin_product_editing_block'>*/}
            {/*  <div className='admin_product_editing_block_row'>*/}
            {/*    <div className='admin_product_editing_select'>*/}
            {/*      <CommonSelect options={TrialSelectOptions} />*/}
            {/*    </div>*/}
            {/*    <div className='admin_product_editing_select'>*/}
            {/*      <CommonSelect options={TrialSelectOptions} />*/}
            {/*    </div>*/}
            {/*    <div className='admin_product_editing_select small'>*/}
            {/*      <LanguageSelect />*/}
            {/*    </div>*/}
            {/*  </div>*/}
            {/*</div>*/}
            <EditProduct currentProduct={currentProduct} />
            <div className="admin_product_editing_block">
              <TextEditor/>
            </div>
            <div className="admin_product_editing_block">
              <div className='input_box admin_product_editing_input_box name'>
                  <h2 className='input_title'>
                    <span className='input_title_text'>Видео-обзор</span>
                  </h2>
                  <input
                    type='text'
                    className='input_box_input'
                    value='https://www.youtube.com/watch?v=oEnvMhvqfXM'
                    readOnly
                  />
                </div>
            </div>
            <EditParams title={'Параметры'} />
            {!!currentProduct.characteristics && currentProduct.characteristics.map((e,i) => (
              <EditParams key={i} title={e.names} value={e.value} characteristics={e}/>
            ))}
            <EditColor />
          </div>
        </section>
        {/*<section className='admin_sidebar_right'>*/}
        {/*  <div className='admin_sidebar_product_preview'>*/}
        {/*    <h2 className='admin_sidebar_product_preview_title'>Превью товара</h2>*/}
        {/*    <div className='admin_sidebar_product_preview_wrap'>*/}
        {/*      <div className='admin_sidebar_product_preview_img'>*/}
        {/*        <img src={bedPreview} />*/}
        {/*      </div>*/}
        {/*      <div className='admin_sidebar_product_preview_info'>*/}
        {/*        <div className='admin_sidevar_product_preview_reviews'>*/}
        {/*          <div className='admin_sidebar_product_preview_stars'>*/}
        {/*            <img src={starActive} />*/}
        {/*            <img src={starActive} />*/}
        {/*            <img src={starActive} />*/}
        {/*            <img src={starActive} />*/}
        {/*            <img src={starActive} />*/}
        {/*            <span className='stars_count'>5</span>*/}
        {/*            <span className='reviews_count'>(9 отзывов)</span>*/}
        {/*          </div>*/}
        {/*          <h2 className='admin_sidebar_product_preview_article'>*/}
        {/*            Артикул: 15259692*/}
        {/*          </h2>*/}
        {/*          <h1 className='admin_sidebar_product_preview_title'>*/}
        {/*            Кровать софия люкс с механизмом*/}
        {/*          </h1>*/}
        {/*          <div className='asmin_sidebar_product_preview_price'>*/}
        {/*            <span className='price_new'>1 500 ₴</span>*/}
        {/*            <span className='price_old'>2 000 ₴</span>*/}
        {/*            <span className='stock'>В наличии</span>*/}
        {/*          </div>*/}
        {/*        </div>*/}
        {/*      </div>*/}
        {/*    </div>*/}
        {/*  </div>*/}
        {/*  <ul className='admin_sidebar_right_colors'>*/}
        {/*    <li className='admin_sidebar_color_item'>*/}
        {/*      <div className='admin_sidebar_color_preview-img'>*/}
        {/*        <img src={productColorPreview} />*/}
        {/*      </div>*/}
        {/*      <div className='admin_sidevar_color_info'>*/}
        {/*        <h1 className='admin_sidebar_color_info_title'>*/}
        {/*          Дуб Ансберг Темный*/}
        {/*        </h1>*/}
        {/*        <div className='admin_sidebar_color_info_price'>*/}
        {/*          <span className='price_new'>1 500 ₴</span>*/}
        {/*          <span className='price_old'>2 000 ₴</span>*/}
        {/*        </div>*/}
        {/*        <div className='admin_sidebar_color_info_count'>*/}
        {/*          <span className='title'>В наличии:</span>*/}
        {/*          <span>2 товара</span>*/}
        {/*        </div>*/}
        {/*        <div className='admin_sidebar_color_info_count'>*/}
        {/*          <span className='title'>Под заказ:</span>*/}
        {/*          <span>21 день</span>*/}
        {/*        </div>*/}
        {/*      </div>*/}
        {/*    </li>*/}
        {/*    <li className='admin_sidebar_color_item'>*/}
        {/*      <div className='admin_sidebar_color_preview-img'>*/}
        {/*        <img src={productColorPreview} />*/}
        {/*      </div>*/}
        {/*      <div className='admin_sidevar_color_info'>*/}
        {/*        <h1 className='admin_sidebar_color_info_title'>*/}
        {/*          Дуб Ансберг Темный*/}
        {/*        </h1>*/}
        {/*        <div className='admin_sidebar_color_info_price'>*/}
        {/*          <span className='price_new'>1 500 ₴</span>*/}
        {/*          <span className='price_old'>2 000 ₴</span>*/}
        {/*        </div>*/}
        {/*        <div className='admin_sidebar_color_info_count'>*/}
        {/*          <span className='title'>В наличии:</span>*/}
        {/*          <span>2 товара</span>*/}
        {/*        </div>*/}
        {/*        <div className='admin_sidebar_color_info_count'>*/}
        {/*          <span className='title'>Под заказ:</span>*/}
        {/*          <span>21 день</span>*/}
        {/*        </div>*/}
        {/*      </div>*/}
        {/*    </li>*/}
        {/*    <li className='admin_sidebar_color_item'>*/}
        {/*      <div className='admin_sidebar_color_preview-img'>*/}
        {/*        <img src={productColorPreview} />*/}
        {/*      </div>*/}
        {/*      <div className='admin_sidevar_color_info'>*/}
        {/*        <h1 className='admin_sidebar_color_info_title'>*/}
        {/*          Дуб Ансберг Темный*/}
        {/*        </h1>*/}
        {/*        <div className='admin_sidebar_color_info_price'>*/}
        {/*          <span className='price_new'>1 500 ₴</span>*/}
        {/*          <span className='price_old'>2 000 ₴</span>*/}
        {/*        </div>*/}
        {/*        <div className='admin_sidebar_color_info_count'>*/}
        {/*          <span className='title'>В наличии:</span>*/}
        {/*          <span>2 товара</span>*/}
        {/*        </div>*/}
        {/*        <div className='admin_sidebar_color_info_count'>*/}
        {/*          <span className='title'>Под заказ:</span>*/}
        {/*          <span>21 день</span>*/}
        {/*        </div>*/}
        {/*      </div>*/}
        {/*    </li>*/}
        {/*  </ul>*/}
        {/*</section>*/}
      </section>
    </section>
  );
};
export default AdminProductEditingPage;
