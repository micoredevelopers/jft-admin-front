import React from 'react';
import { redCross, dollar } from '../../assets/img';

const EditParams = ({title, value, characteristics}) => {
	return (
		<div className="admin_product_editing_block">
			<div className="admin_product_editing_row_title">
				<h1 className="title">{title}</h1>
				<button className="btn">Добавить</button>
			</div>
			<div className="admin_product_editing_block_row">
				<div className="input_box admin_product_editing_input_box name">
					<h2 className="input_title">
						<span className="input_title_text">Название</span> <span className="input_title_star">*</span>
					</h2>
					<input type="text" className="input_box_input" value="Ширина"/>
				</div>
				{!!characteristics ?
					<div className="input_box admin_product_editing_input_box name">
						<h2 className="input_title">
						<span className="input_title_text">Цена</span> <span className="input_title_star">*</span>
						</h2>
						<input type="text" className="input_box_input" value={'100'}/>
						 <img className="input_box_img" src={dollar} alt=""/>
					</div> :
					<div className="input_box admin_product_editing_input_box name">
						<h2 className="input_title">
						<span className="input_title_text">Значение</span> <span className="input_title_star">*</span>
						</h2>
						<input type="text" className="input_box_input" value="310 см"/>
					</div>
				}
				<div className="admin_product_editing_delete_btn">
					<img src={redCross} alt=""/>
				</div>
			</div>
		</div>
	);
};
export default EditParams;
