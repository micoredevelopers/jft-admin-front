import React from 'react';
import { Field } from 'formik';
import { closeOrDelete, plusOrange } from '../../assets/img';
import { Form } from 'formik';
import { Formik } from 'formik';
import { fileSchema } from './../../YupSchemas/fileSchema';
import * as Yup from 'yup';

const formikImgInitValues = {img: ''};
const formikImgValidation = Yup.object({
	img: fileSchema,
});

const EditProduct = ({currentProduct}) => {
	return (
		<div className="admin_product_editing_block">
			<div className="admin_product_editing_block_row">
				<div className="input_box admin_product_editing_input_box name">
					<h2 className="input_title">
						<span className="input_title_text">Название</span> <span className="input_title_star">*</span>
					</h2>
					<input
						type="text"
						className="input_box_input"
						defaultValue={currentProduct.names}
					/>
				</div>
				<div className="input_box admin_product_editing_input_box">
					<h2 className="input_title">
						<span className="input_title_text">Артикул</span> <span className="input_title_star">*</span>
					</h2>
					<input
						type="text"
						className="input_box_input"
						value="15259692"
					/>
				</div>
			</div>
			<div className="admin_product_editing_block_row">
				<div className="input_box admin_product_editing_input_box file">
					<Formik
						initialValues={formikImgInitValues}
						validationSchema={formikImgValidation}>
						{({setFieldValue, errors, touched, values}) => (
							<Form>
								<h2 className="input_title">
                            <span className="input_title_text">
                              Заглавное фото
                            </span> <span className="input_title_star">*</span>
								</h2>
								<div className="multiple_file_upload_box">
									<div className="multiple_file_upload_files">
										<div className="multiple_file_upload_file">
											<span>{values.img.name}</span> <img src={closeOrDelete} alt=""/>
										</div>
									</div>

									<label className="multiple_file_upload_btn"> <img src={plusOrange}/> <Field
										name="image"
										type="file"
										onChange={(e) => {
											setFieldValue('img', e.target.files[0]);
										}}
									/> </label>
								</div>
								{!!errors.img && <div className="error_message">{errors.img}</div>}
							</Form>
						)}
					</Formik>
				</div>
				<div className="input_box admin_product_editing_input_box">
					<h2 className="input_title">
						<span className="input_title_text">Артикул</span> <span className="input_title_star">*</span>
					</h2>
					<input
						type="text"
						className="input_box_input"
						value="15259692"
					/>
				</div>
				<div className="input_box admin_product_editing_input_box">
					<h2 className="input_title">
						<span className="input_title_text">Цена</span> <span className="input_title_star">*</span>
					</h2>
					<input
						type="text"
						className="input_box_input"
						defaultValue={currentProduct.price && currentProduct.price[0].unit_price}
					/> <span className="admin_product_editing_input_box_icon">
                    ₴
                  </span>
				</div>
				<div className="input_box admin_product_editing_input_box">
					<h2 className="input_title">
						<span className="input_title_text">Скидка</span> <span className="input_title_star">*</span>
					</h2>
					<input
						type="text"
						className="input_box_input"
						value="500"
					/> <span className="admin_product_editing_input_box_icon">
                    ₴
                  </span>
				</div>
			</div>
			<div className="admin_product_editing_block_row">
				<div className="input_box multiple_file_upload admin_product_multiple_file_upload ">
					<Formik
						initialValues={formikImgInitValues}
						validationSchema={formikImgValidation}>
						{({setFieldValue, errors, touched, values}) => (
							<Form>
								<h2 className="input_title">
                              <span className="input_title_text">
                                Дополнительные фото
                              </span> <span className="input_title_star">*</span>
								</h2>
								<div className="multiple_file_upload_box">
									<div className="multiple_file_upload_files">
										<div className="multiple_file_upload_file">
											<span>{values.img.name}</span> <img src={closeOrDelete} alt=""/>
										</div>
									</div>

									<label className="multiple_file_upload_btn"> <img src={plusOrange}/> <Field
										name="image"
										type="file"
										onChange={(e) => {
											setFieldValue('img', e.target.files[0]);
										}}
									/> </label>
								</div>
								{errors.img ? (
									<div className="error_message">{errors.img}</div>
								) : (
									<></>
								)}
							</Form>
						)}
					</Formik>
				</div>
			</div>
		</div>
	);
};
export default EditProduct;