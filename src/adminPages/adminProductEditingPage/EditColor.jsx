import React from "react";
import { closeOrDelete, dollar, plusOrange, redCross } from '../../assets/img';
import { Field } from 'formik';
import { Form } from 'formik';
import { fileSchema } from './../../YupSchemas/fileSchema';
import { Formik } from 'formik';
import * as Yup from "yup";
const formikImgInitValues = { img: "" };
const formikImgValidation = Yup.object({
  img: fileSchema,
})

const EditColor = () => {
  return (
    <div>
      <div className='admin_product_editing_block'>
        <div className='admin_product_editing_row_title'>
          <h1 className='title'>Цвет корпуса</h1>
          <button className='btn'>Добавить</button>
        </div>
        <div className='admin_product_editing_block_row'>
          <div className='input_box admin_product_editing_input_box file name'>
            <Formik
              initialValues={formikImgInitValues}
              validationSchema={formikImgValidation}>
              {({ setFieldValue, errors, touched, values }) => (
                <Form>
                  <h2 className='input_title'>
                    <span className='input_title_text'>Фото</span>
                    <span className='input_title_star'>*</span>
                  </h2>
                  <div className='multiple_file_upload_box'>
                    <div className='multiple_file_upload_files'>
                      <div className='multiple_file_upload_file'>
                        <span>{values.img.name}</span>
                        <img src={closeOrDelete} alt='' />
                      </div>
                    </div>

                    <label className='multiple_file_upload_btn'>
                      <img src={plusOrange} />
                      <Field
                        name='image'
                        type='file'
                        onChange={(e) => {
                          setFieldValue("img", e.target.files[0]);
                        }}
                      />
                    </label>
                  </div>
                  {errors.img ? (
                    <div className='error_message'>{errors.img}</div>
                  ) : (
                    <></>
                  )}
                </Form>
              )}
            </Formik>
          </div>
          <div className='input_box admin_product_editing_input_box name'>
            <h2 className='input_title'>
              <span className='input_title_text'>Название</span>
              <span className='input_title_star'>*</span>
            </h2>
            <input
              type='text'
              className='input_box_input'
              value='Дуб Ансберг Темный'
            />
          </div>
          <div className="input_box admin_product_editing_input_box name">
            <h2 className="input_title">
              <span className="input_title_text">Цена</span> <span className="input_title_star">*</span>
            </h2>
            <input type="text" className="input_box_input" value={'100'}/>
            <img className="input_box_img" src={dollar} alt=""/>
          </div>
          <div className="admin_product_editing_delete_btn">
            <img src={redCross} alt=""/>
          </div>
        </div>
      </div>
    </div>
  );
};
export default EditColor;
