import * as Yup from "yup";

export const signInSchema = Yup.object({
  login: Yup.string().required('* обязательное поле'),
  password: Yup.string().required('* обязательное поле')
})