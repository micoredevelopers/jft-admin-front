import * as Yup from "yup";
import { fileSchema } from "./fileSchema";

export const newOfferSchema = {
  withCategory: Yup.object({
    subcategory: Yup.string().required("* обязательное поле"),
    desc: Yup.string().required("* обязательное поле"),
    stop_date: Yup.date().typeError('* введите дату').required("* введите дату"),
    start_date: Yup.date().typeError('* введите дату').required("* введите дату"),
    title: Yup.string().required("* обязательное поле"),
    image: fileSchema,
  }),
  withoutCategory: Yup.object({
    desc: Yup.string().required("* обязательное поле"),
    stop_date: Yup.date().typeError('* введите дату').required("* введите дату"),
    start_date: Yup.date().typeError('* введите дату').required("* введите дату"),
    title: Yup.string().required("* обязательное поле"),
    image: fileSchema,
  }),
}