import * as Yup from "yup";

export const orderCardSchema = Yup.object({
  first_name: Yup.string().required('* обязательное поле'),
  last_name: Yup.string().required('* обязательное поле'),
  middle_name: Yup.string().required('* обязательное поле'),
  city: Yup.string().required("* Выберите город"),
  warehouse: Yup.string().required("* Выберите почтовое отделение"),
})