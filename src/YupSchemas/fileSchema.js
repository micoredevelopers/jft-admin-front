import * as Yup from "yup";

export const fileSchema = Yup.mixed()
  // .required("* загрузите лого")
  .test("fileSize", "* слишком большой файл", (value) => {
    if(value && value.size <= 6000000){
      return true;
    }else if(typeof value === 'string'){
      if(value.includes('/storage/images/')){
        return true;
      }
    }
    return false;
  })
  .test(
    "type",
    "* Поддерживаются только форматы: .png, .jpg, .webp",
    (file) => {
      return fileTypeValidation(file);
    }
  );

export const fileTypeValidation = (file) => {
  if (["image/png", "image/jpeg", "image/webp", "image/svg+xml"].includes(file && file.type)) {
    return true;
  }else if(file && !file.type && file.includes('storage/images/')){
      return true
    }
  return false;
};
