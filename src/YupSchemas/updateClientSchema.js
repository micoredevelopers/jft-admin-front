import * as Yup from "yup";

export const updateClientSchema = {
  password: Yup.object({
    newPassword: Yup.string()
      .min(8, "* пароль должен содержать не меньше 8 символов")
      .required("* обязательное поле"),
    newPasswordConfirmation: Yup.string()
      .oneOf([Yup.ref("newPassword")], "* пароли не совпадают")
      .required("* обязательное поле"),
  }),
  email: Yup.object({
    email: Yup.string().nullable().email('* ведите корректный email адрес').required("* Введите свой email адрес"),
  }),
};
