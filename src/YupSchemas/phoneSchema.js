import * as Yup from "yup";

export const phoneSchema = Yup.string()
  .matches(
    /^((\+?3)?8)?((0\(\d{2}\)?)|(\(0\d{2}\))|(0\d{2}))\d{7}$/,
    "* введите корректный номер телефона"
  )
  .required("* обязательное поле");
