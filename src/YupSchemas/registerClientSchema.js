import * as Yup from "yup";
import { phoneSchema } from "./phoneSchema";

export const registerClientSchema = {
  virtual: Yup.object({
    name: Yup.string().required("* обязательное поле"),
    phone: phoneSchema,
    password: Yup.string()
      .min(8, "* пароль должен содержать не меньше 8 символов")
      .required("* обязательное поле"),
    password_confirmation: Yup.string()
      .min(8, "* пароль должен содержать не меньше 8 символов")
      .oneOf([Yup.ref("password")], "* пароли не совпадают")
      .required("* обязательное поле"),
  }),

  phisical: Yup.object({
    name: Yup.string().required("* обязательное поле"),
    phone: phoneSchema,
    password: Yup.string()
      .min(8, "* пароль должен содержать не меньше 8 символов")
      .required("* обязательное поле"),
    password_confirmation: Yup.string()
      .min(8, "* пароль должен содержать не меньше 8 символов")
      .oneOf([Yup.ref("password")], "* пароли не совпадают")
      .required("* обязательное поле"),
    first_name: Yup.string().required("* обязательное поле"),
    last_name: Yup.string().required("* обязательное поле"),
    middle_name: Yup.string().required("* обязательное поле"),
    city: Yup.string().required("* Выберите город"),
    warehouse: Yup.string().required("* Выберите почтовое отделение"),
  }),
};
