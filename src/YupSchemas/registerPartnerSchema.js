import * as Yup from "yup";
import { phoneSchema } from "./phoneSchema";
export const registerPartnerSchema = {
  withCategory: Yup.object({
    title: Yup.string().required("* обязательное поле"),
    addresses: Yup.array().required("* обязательное поле"),
    password: Yup.string()
      .min(8, "* пароль должен содержать не меньше 8 символов")
      .required("* обязательное поле"),
    password_confirmation: Yup.string()
      .oneOf([Yup.ref("password")], "* пароли не совпадают")
      .required("* обязательное поле"),
    contact_name: Yup.string().required("* обязательное поле"),
    contact_phone: phoneSchema,
    category: Yup.string().required("* обязательное поле"),
    cities: Yup.mixed().required("* Введите город"),
  }),
  withoutCategory: Yup.object({
    title: Yup.string().required("* обязательное поле"),
    addresses: Yup.string().required("* обязательное поле"),
    password: Yup.string()
      .min(8, "* пароль должен содержать не меньше 8 символов")
      .required("* обязательное поле"),
    password_confirmation: Yup.string()
      .oneOf([Yup.ref("password")], "* пароли не совпадают")
      .required("* обязательное поле"),
    contact_name: Yup.string().required("* обязательное поле"),
    contact_phone: phoneSchema,
    cities: Yup.mixed().required("* Введите город"),
  }),
};
